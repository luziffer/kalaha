#!/usr/bin/env python3

class Kalaha():
    
    # setup field
    def __init__(self):
        self.fields = [4] * 12
        self.scores = [0]* 2
        self.turn = 0
    
    def draw(self):
        print("\n")
        player = self.turn%2
        names = ["bottom", "top"]
        print("Round %d, turn of player %d (%s)" % (self.turn/2, player+1, names[player]) )
        print("-" * 18)
        print(" ".join(["%2d" % s for s in self.fields[6:][::-1]]))
        print("  %2d         %2d " % (self.scores[1], self.scores[0]) )
        print(" ".join(["%2d" % s for s in self.fields[:6]]))
        print("-" * 18)
        
    def move(self, field):
        player = field >= 6
        assert player == (self.turn % 2), \
            "Player %d cannot start from side of other player." % (player+1)
        
        stones = self.fields[field]
        assert stones > 0, "Field %d is empty." % field 
        
        # move ends in storage?
        storage = 6 if player==0 else 12
        if (storage - (field+stones))%12 != 0:
            self.turn += 1

        # subtract stones that give points
        for k in range(4):
            if stones >= storage - field + 12*k:
                stones -= 1
                self.scores[player] += 1
        
        # distribute stones on field
        self.fields[field] = 0
        for s in range(stones):
            self.fields[(field+s+1)%12] += 1
        
        # ended on an empty field
        last_field = (field+stones)%12
        if self.fields[last_field] == 1:
            can_take = False
            if player==0 and last_field < 6:
                can_take = True
            elif player==1 and last_field >= 6:
                can_take = True
            if can_take:
                opposite = 11 - last_field
                self.scores[player] += self.fields[opposite]
                self.fields[opposite] = 0
                
        # distribute remaining stones 
        if self.finished():
            remainder = sum(self.fields)
            self.scores[(player+1)%2] += remainder
            self.fields = [0] * 12
    
    def finished(self):
        player = self.turn % 2
        if player == 0: fields = self.fields[:6]
        else: fields = self.fields[6:]
        return all([f==0 for f in fields])
    
    def play(self):
        while not self.finished():
            self.draw() 
            
            # ask for field
            player = self.turn % 2
            moved = False
            while not moved:
                field = None
                while field is None:
                    choice = input("Choose a field from 1 to 6: ") 
                    try: 
                        choice = int(choice)
                        if 1 <= choice and choice <= 6:
                            field = choice - 1
                        else:
                            print("Field must be between 1 and 6!")
                    except ValueError:
                        print("Please input a number.")
                if player == 1: field += 6
                
                # try to execute move
                try:
                    self.move(field)
                    moved = True
                except AssertionError as e:
                    print("Move invalid! %s" % e)
                        
        # feedback on who won
        if self.scores[0] > self.scores[1]:
            print("Player 1 won!")
        elif self.scores[0] < self.scores[1]:
            print("Player 2 won!")
        else:
            print("Draw, both players won.")   
        
if __name__ == "__main__":
    game = Kalaha()
    game.play()