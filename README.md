# Kalaha in Python

Also known as [Kalah](https://en.wikipedia.org/wiki/Kalah), the game came can be solved exhaustively by tree search.

This is a simple pure Python implentation. Run `python kalaha.py` in the terminal.

### Example 

Here is a common opening strategy for Kalah(6,4) in which the first player exploits the "move-again" rule. 

```
Round 0, turn of player 1 (bottom)
------------------
 4  4  4  4  4  4
   0          0 
 4  4  4  4  4  4
------------------
Choose a field from 1 to 6: 3


Round 0, turn of player 1 (bottom)
------------------
 4  4  4  4  4  4
   0          1 
 4  4  0  5  5  5
------------------
Choose a field from 1 to 6: 5


Round 0, turn of player 2 (top)
------------------
 4  4  4  5  5  5
   0          2 
 4  4  0  5  0  6
------------------
Choose a field from 1 to 6: 2


Round 0, turn of player 2 (top)
------------------
 5  5  5  6  0  5
   1          2 
 4  4  0  5  0  6
------------------
Choose a field from 1 to 6:  ...?
```





```
